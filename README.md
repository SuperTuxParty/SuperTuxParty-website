# Welcome to the Repository for the SuperTuxParty Website 👋

[![License: GPL](https://img.shields.io/badge/License-GPL-yellow.svg)](https://gitlab.com/supertuxparty/supertuxparty-website/-/blob/master/LICENSE)

> Website for SuperTuxParty made with [Hugo](https://gohugo.io/)

### 🏠 [Online Website](https://supertux.party/)

### ✨ [SuperTuxParty Repository](https://gitlab.com/SuperTuxParty/SuperTuxParty)

## Install

First fork this project.
Than clone your fork and the Git submodule to your computer.

```sh
git clone --recurse-submodules YOUR_FORK_HTTPS_ADDRESS
```

and install [Hugo](https://gohugo.io/)

## Usage Documentation

[Hugo Documentation](https://gohugo.io/documentation/)  
[Ananke Theme Documentation](https://github.com/theNewDynamic/gohugo-theme-ananke)

## Start the Hugo server

Run these command at the root folder of the Repository

```sh
hugo server
```

and visit http://localhost:1313/

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/SuperTuxParty/SuperTuxParty-website/-/issues). You can also take a look at the [contributing guide](https://gitlab.com/SuperTuxParty/SuperTuxParty-website/-/issues).

## Show your support

Give a ⭐️ if you like this project !

## 📄 Code used in this project

The Website uses the [Ananke theme](https://github.com/theNewDynamic/gohugo-theme-ananke), which is licensed under the [MIT](https://github.com/theNewDynamic/gohugo-theme-ananke/blob/master/LICENSE.md) license

The Downloads buttons on the download page are Copyright (c) 2021 by [jesus tapial](https://codepen.io/machuenca/pen/yNLEPL), which are licensed under the [MIT License](https://gitlab.com/supertuxparty/supertuxparty-website/-/blob/master/content/english/download_button_license.md). The source code svg icon on the download page is part of [FontAwesome](https://fontawesome.com/) and licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).

The RSS icon on the RSS page is from unnamed (Mozilla Foundation) - feedicons.com, GPL 2.0 or any later version, https://commons.wikimedia.org/w/index.php?curid=654420
and is downloaded from [Wikipedia Commons](https://commons.wikimedia.org/wiki/File:Feed-icon.svg)

The GitLab icon is licensed under the [CC BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/) by GitLab.

## 📝 License

This project is under the [GNU GPL v3](https://gitlab.com/SuperTuxParty/SuperTuxParty-website/-/blob/master/LICENSE) licensed.
