---
title: Über
draft: false
featured_image: "/img/6c4d80e321d03931f687072f681955844d8eca7a8b56cd8ef58842a1fb30eed0.webp"
---

## Super Tux Party

Das Code-Repository wird auf [GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty) gehostet.  
Wenn Sie einen Fehler im Spiel gefunden haben oder eine Idee zu einer Funktion haben, öffnen Sie bitte ein Problem im [Issue Tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/issues).  
Weitere Informationen finden Sie in der [README](https://gitlab.com/SuperTuxParty/SuperTuxParty) Datei.

## Webseite

Diese Webseite ist unter der [GNU GPL v3 Lizenz](https://gitlab.com/SuperTuxParty/SuperTuxParty-website/-/blob/master/LICENSE).  
Alle Dateien für diese Webseite befinden sich auf [GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty-website).

Die Website wurde mit [Hugo](https://gohugo.io/) erstellt mit dem [Ananke Thema](https://github.com/theNewDynamic/gohugo-theme-ananke).

Die Download-Schaltflächen auf der Download-Seite unterliegen dem Copyright (c) 2021 von [jesus tapial](https://codepen.io/machuenca/pen/yNLEPL) unter der [MIT Lizenz](/download_button_license/). Das Quelltext SVG-Symbol auf der Download-Seite ist Bestandteil von [FontAwesome](https://fontawesome.com/) und lizensiert unter [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).

Das Feed-Symbol auf der RSS-Seite stammt von unbenannt (Mozilla Foundation) - feedicons.com, GPL 2.0 oder eine spätere Version, https://commons.wikimedia.org/w/index.php?curid=654420
und ist heruntergeladen von [Wikipedia Commons](https://commons.wikimedia.org/wiki/File:Feed-icon.svg)

Das GitLab-Symbol ist unter der [CC BY-NC-SA 4.0-Lizenz](https://creativecommons.org/licenses/by-nc-sa/4.0/) von GitLab lizensiert.

---
## Verantwortlichkeit nach § 5 TMG

**Name**: Florian Kothmeier

**Addresse**: Fahrstraße 6,  
91054 Erlangen, Deutschland

**E-mail**: [info@supertux.party](mailto:info@supertux.party)
