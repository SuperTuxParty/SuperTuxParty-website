---
description: "Ein Freies und Open-Source Partyspiel."
---

#### **HINWEIS: Dieses Spiel befindet sich derzeit in der ALPHA-Phase**

Ein [Freies](https://www.gnu.org/philosophy/free-sw.html) und
[Open-Source](https://opensource.org/docs/osd/) Partyspiel, das das Gefühl von Spielen wie Mario Party nachbilden soll.

Das Code-Repository für [das Spiel](https://gitlab.com/SuperTuxParty/SuperTuxParty) und für [die Seite](https://gitlab.com/SuperTuxParty/SuperTuxParty-website) ist auf GitLab
