---
title: "Alpha v0.2 veröffentlicht"
type: "Post"
date: 2018-10-31T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Nach zwei Monaten Arbeit ist unser neues Update endlich da! Es enthält viele neue Funktionen und Änderungen, einschließlich eines neuen spielbaren Charakters, Godette. Es wurden auch drei neue Minispiele und neue Spielmodi hinzugefügt, was aufregend ist. Probieren Sie es aus! Derzeit werden Sie feststellen, dass uns einige Assets fehlen und wir auf der Suche nach einem Künstler sind. Wenn Sie helfen möchten oder eine Person kennen, die uns helfen könnte, wenden Sie sich bitte über [GitLab](https://gitlab.com/supertuxparty/supertuxparty) oder unseren [Matrix-Kanal](https://matrix.to/#/!hDKeDHVhkMRnxkNyga:matrix.org) an uns.

Wenn Sie Probleme haben, melden Sie diese bitte in unserem [Issue Tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/issues)!

## **Änderungen für v0.2**

**Neue Funktionen**

- Godette, Godots inoffizielles Maskottchen, als neue spielbare Figur **_!53_**
- Animationen für alle Charaktere **_!25_**
- Bowling Minispiel, ein Spieler versucht die 3 anderen Spieler zu treffen **_!63_**
- Kernel Compiling minigame, press the correct buttons quickly **_!52_**
- Erntezeit Minispiel,ein Ratespiel **_!39_**
- Boards can now have multiple paths players can choose which way to go **_!26_**
- Minigame information screens **_!28_**
- New gamemodes added such as, Duel, 1v3 and 2v2 **_!64_**
- Games can now be saved **_!33_**

**Verbesserungen**

- Options are now saved **_!54_**
- Fixed a memory leak **_!29_**
- Improved mesh and texture for ice in Knock Off minigame **_!30_**, **_!34_**
- Hurdle minigame now has powerups and different hurdles **_!38_**

Besonderer Dank geht an RiderExMachina, [Yeldham](https://yeldham.itch.io/) und Dragoncraft89, ohne sie wäre dieses Projekt in so kurzer Zeit nie so weit gekommen!

