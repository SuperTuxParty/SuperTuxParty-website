---
title: "Alpha v0.4 veröffentlicht"
type: "Post"
date: 2019-06-08T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Dieses Update verbessert verschiedene Aspekte von Super Tux Party. Wir haben uns darauf konzentriert, fehlende Elemente wie Hintergründe zum Spiel hinzuzufügen.  
Abgesehen davon haben wir Übersetzungen, Musik im Hauptmenü und einige Fehlerbehebungen bekommen.  
Wenn Sie Verbesserungsvorschläge haben, Kunst für das Spiel schaffen oder auf andere Weise einen Beitrag leisten möchten, besuchen Sie uns auf Gitlab.

## **Alpha Version - v0.4**

**Neue Funktionen**

- Unterstützung für die Lokalisierung (außer Plugins)
  - Derzeit unterstützte Sprachen:
    - Englisch
    - Brasilianisches Portugiesisch
    - Deutsch
    - Französisch
- Teamanzeige in 2v2-Minispielen

**Verbessert / geändert**

- Das Hauptmenü kann jetzt mit Tastatur / Controller navigiert werden
- Das Brett-Overlay zeigt nun die Gegenstände jedes Spielers
- Computergesteuerte Charaktere kaufen jetzt Artikel im Shop
- Musik im Hauptmenü hinzugefügt
- Behebung eines Fehlers, durch den Gegenstände in Spielen, die aus Speicherständen geladen wurden, nicht verwendet werden konnten
- Es wurden Texturen für das Erntezeit Minispiel und die Platzierungsszene hinzugefügt

Intern wurde das Projekt auf die neue Godot-Version 3.1 umgestellt

