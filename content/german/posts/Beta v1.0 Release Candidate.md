---
title: "Beta v1.0 Release Candidate"
date: 2022-06-26T12:00:00+02:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/cf54ee509ea4aa91cf60afbb32aa3251a6761af47c2e2559a075de3a3d877173.webp"
---

Nach langer Pause sind wir endlich zurück und können unseren Fortschritt an SuperTuxParty zeigen. Es ist zwar noch kein fertiges Release, aber wir fühlen uns sicher genug um Feeback zu bekommen.

Also was hat eigentlich so lange gedauert? Die Antwort lautet: Online Multiplayer. Jetzt könnt ihr endlich mit euren Freunden am anderen Ende der Welt spielen!

Außerdem haben wir unsere UI angepasst, damit es mit online multiplayer funktioniert:

![multiplayer lobby](/img/cf54ee509ea4aa91cf60afbb32aa3251a6761af47c2e2559a075de3a3d877173.webp)

Wenn ihr es ausprobieren wollt, geht zu unserer [Download Seite](/de/download/latest) und ladet die neue Version herunter. Wir habein einen Server unter `supertux.party` aufgesetzt, den ihr nutzen könnt.

Wenn ihr euren SuperTuxParty Server lieber selber aufsetzt, könnt ihr die neue Download Option "server" nutzen um euch das server binary herunterzuladen.

Solltet ihr Bugs finden, dann immer her damit, damit wir diese beheben können! Am besten erstellt ihr dafür ein Issue in unserem [GitLab Issue Tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/issues)

