---
title: "Alpha v0.9 veröffentlicht"
type: "Post"
date: 2021-04-19T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/48b9e74e8df51e6670f3c8d351d28e2b0005e60e590457a8649aa7a028a9ccb0.webp"
---

Wir sind zurück mit einem neuen Super Tux Party Release.

Diese Version konzentriert sich darauf, das Spiel zu verbessern, anstatt eine Menge neuer Funktionen zu bieten.

Der Grund dafür ist, dass wir mit der Arbeit am Online-Multiplayer beginnen werden, da dies einer unserer allerersten Feature-Anfragen war und wir jetzt an einem Punkt sind, an dem es sinnvoll ist, ihn zu implementieren. Da dies ein großes Feature ist, möchten wir das Spiel dafür in einem guten Zustand haben.

Werfen wir nun einen Blick auf die großen Änderungen in dieser Version.

Wir haben den Bildschirm vor den Minispielen mit einem stark verbesserten Design überarbeitet.

![Neuer Bildschirm vor den Minispielen](/img/7f23c35e32d03a9b1488be622910b7bcf7093e7edd0ddd4075364fdd43ae115e.webp)

Neben einem stark verbesserten Layout zeigt es Ihnen auch, welchen Minispieltyp Sie spielen werden, die Teamfarben und zeigt nur relevante Steuerelemente für 1v3-Minispiele an.

Wir haben auch den Minispiel-Endbildschirm, den Sie sehen, nachdem Sie ein Minispiel gespielt haben, neu gestaltet.

![Neu gestalteter Minispiel-Endbildschirm](/img/eb1e5d76c21723b5bdcc23a88048290cc7742400e7fc92893448560b8c29ed3a.webp)

Es ist optisch viel ansprechender und Sie können die Belohnungen sehen, die jeder Spieler für seine Platzierung erhalten hat. Der Bildschirm geht auch einfach weiter, wenn alle Spieler bereit sind. Sie haben also so viel Zeit, wie Sie brauchen, um sich die Statistiken anzusehen.

Wir haben auch ein neues Hauptmenü, um das alte zu ersetzen, um dem Spiel mehr Charakter zu verleihen.

![Neues Hauptmenü](/img/48b9e74e8df51e6670f3c8d351d28e2b0005e60e590457a8649aa7a028a9ccb0.webp)

Und auch ein komplett neuer Look für das Minispiel "Kernel-Kompilierung".

![Neues Aussehen](/img/8aa7a7d83687fe6fcb479d5ade116a7bbbbdb511669fe5296cf94066e4c61bbc.webp)

Wir haben auch die Funktionsweise der 2v2-Variante hier geändert. Anstatt dass jeder Spieler seinen eigenen Balken füllt, hat jedes Team nun nur noch einen Balken, den es füllen muss. Um diese Variante in ein echtes Teamspiel zu verwandeln.

Auch ein großes Lob an Franzopow, für die Musik im Minispiel „Kernel-Kompilierung“ sowie dem Bildschirm nach den Minispielen. Wenn Ihnen seine Arbeit gefällt, sollten Sie ihn bei [Paypal](https://www.paypal.com/donate?hosted_button_id=SZUUK34MBU944) unterstützen.

Hier wie immer das komplette Changelog:

## **Changelog v0.9**

**Verbessert / geändert**

- Der Endbildschirm der Minispiele wurde neu gestaltet
- Neues Kuchendesign
- Hidpi-Unterstützung aktiviert
- Timer für Dungeon Parkour und Forest Run hinzugefügt
- Behoben, dass die KI im Minispiel Escape from Lava stecken blieb
- Minigame-QoL-Änderungen:
  - Die Zeit für die Pflanzenauswahl im Minispiel "Lebensmittel Ernten" wurde von 10 auf 8 Sekunden verkürzt
  - Spawne weniger Bomben im Minispiel "Boat Rally"
  - Vorstart-Countdown in den Minispielen „Escape from Lava“, „Lebensmittel Ernten“ und „Hurdle“ hinzugefügt
  - Minispiel-Screenshots verbessert
  - Bessere Standard-Tastaturbelegungen
- Problem behoben, bei dem das Charaktermodell von Beastie und Godette in den Boden geclippt wurde
- Screenshot-Erstellung unter Windows funktioniert jetzt
- Neues Hauptmenü-Design und Animationen
- Neuer Bildschirm vor den Minispielen
- UI Thema Verbesserungen
- Das Credits-Menü gehorcht jetzt wie vorgesehen dem Thema
- Problem behoben, bei dem das Minispiel „Lebensmittel Ernten“ die Fähigkeit zum Springen auflistete, während das Minispiel dies nicht zulässt
- Zeigen Sie die Steuerelemente, um eine Karte im "Memory" Minispiel umzudrehen
- Das "Kernel-Kompilierungs-Minispiel" wurde überarbeitet
  - Der visuelle Stil entspricht jetzt dem gewünschten Cartoon-Look
  - In der 2v2-Version füllen Teamspieler jetzt denselben Balken statt separater Balken
- Spielersymbole werden jetzt im Minispiel "Memory" korrekt angezeigt
- Fehlerhaftes Verhalten beim Aufdecken einer Karte im Minispiel „Memory“ behoben
- Das Minispiel "Memory" kehrt jetzt nicht sofort zum Endbildschirm des Minispiels zurück, sondern gibt dir etwas Zeit, um dir die Statistiken zu betrachten
- Verwende Liberation Sans anstelle von Noto Sans als Fallback-Schriftart
- Zeige die Gesamtzahl der Runden auf den Brett an
- Die Hintergrundmusik auf dem Testboard wiederholt jetzt
- Das Spiel spielt jetzt einen Ton ab, wenn Sie ein Feld auf dem Brett passieren
