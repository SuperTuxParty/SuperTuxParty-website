---
title: "Alpha v0.7 veröffentlicht"
type: "Post"
date: 2020-04-01T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/0c020c0cc6c9d46198739a101d6217f12635b043815d6aa65ef87e88ebbcbd5b.webp"
---

Heute ist vielleicht der erste April, aber das ist kein Scherz:  
**Es gibt eine neue Super Tux Party Version!**

Wie in der vorherigen Version angekündigt, enthält diese Version eine erheblich verbesserte Version von GNU Feldern sowie ein neues Feld: Nolok-Feld.

## **Wie funktionieren sie?**

### **GNU Felder**

![GNU Felder](/img/0c020c0cc6c9d46198739a101d6217f12635b043815d6aa65ef87e88ebbcbd5b.webp)

Bei der Landung auf einem GNU-Feld wird ein Minispiel gespielt. Es gibt zwei Arten von Minispielen:

- Solo Minispiele: Eine Einzelspieler-Herausforderung. Wenn der Spieler erfolgreich ist, erhält der Spieler einen Gegenstand als Belohnung.
- Coop Minispiele: Hier müssen sich die Spieler zusammenschließen. Wenn sie erfolgreich sind, erhalten sie jeweils 10 Kekse.

GNU Felder helfen Ihnen und allen anderen Spielern.

### **Nolok Felder**

![Nolok Felder](/img/87c6c8c5d5cc42506d2052355c81f3c29d0a3378a540adb35bb1de90a7fc67a6.webp)

Nolok Felder sind das Gegenteil von GNU-Feldern. Wenn GNU-Felder die Spieler belohnen, bestrafen Nolok-Felder sie stattdessen.

Nolok Felder rufen auch beim Landen unterschiedliche Effekte hervor:

- Solo Minispiele: Sehr ähnlich der GNU-Variante, nur anstatt den Spieler mit einem Gegenstand zu belohnen. Sie verlieren einen Kuchen, wenn sie das Minispiel nicht bestehen.
- Coop-Minispiele: Ähnlich wie die GNU-Variante, aber die Spieler erhalten keine Kekse, wenn sie erfolgreich sind. Sie verlieren Kekse, wenn sie versagen.
- Bretteffekt: Im Gegensatz zu den GNU-Feldern können Nolok-Felder dem Spieler eine Strafe geben, ohne ein Minispiel zu spielen. Derzeit gibt es zwei Arten von Strafen:
  - Münzen stehlen: Nolok stiehlt den Spielern je nach Platzierung Münzen
  - Schlechte Würfe: Nolok kann auch die Würfelwürfe von Spielern verschlechtern. In diesem Fall sind Ihre Würfelwürfe in den nächsten 5 Runden bei jedem Wurf 2 weniger.

Nolok-Felder haben also eine ziemlich schlechte Auswirkungen, wenn Sie auf sie landen. Bleib lieber weg von ihnen!

## **Neue Minispiele**

Da wir neue Minispieltypen eingeführt haben, benötigen wir natürlich Minispiele, um diese neuen Typen zu implementieren! Die vorherige Version von Gnu-Minispielen heißt jetzt GNU Coop-Minispiele. Es gibt also drei neue Minispiele, über die wir diskutieren müssen.

### **Waldlauf (GNU Solo):**

![Waldlauf](/img/837496cd40cdf6b1caa0b6007b4a5bd3d93f3aad183f60664285ca96ecde1f7f.webp)

Dies ist ein Minispiel im Jump and Run-Stil, bei dem Sie den von Katapulten geworfenen Steinen ausweichen und zwischen Plattformen springen müssen, um die Ziellinie zu erreichen.

### **Verlies Parkour (Nolok Solo):**

![Verlies Parkour](/img/0c91298184bca27770e6fc0d73b93c3db4a53a735d2442cbeeb5e51deeafd2d9.webp)

Ähnlich wie das Waldlaufen Minispiel ist auch dieses Minispiel ein Minispiel im Jump and Run-Stil aber anstatt Steinen ausweichen müssen Sie Feuerbällen ausweichen.

### **Bootsrallye (Nolok Coop):**

![Bootsrallye](/img/5c221ccd2c0f6710958dc9d986ed5dea8bf1d61be82d272a609920fe6e67fbf6.webp)

In diesem Minispiel müssen Sie mit Ihrem Boot die Ziellinie erreichen. Der Haken ist, dass jeder Spieler nur an seiner Position paddeln kann.
Deshalb ist Teamwork hier wichtig.

## **Übersetzungen**

Wir haben freundlicherweise eine kostenlose Hosting-Lösung für die Übersetzung auf [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party/) erhalten.Übersetzungen sind offen und willkommen, bitte helfen Sie, wenn Sie können!

## **Änderungen v0.7**

**Neue Funktionen**

- Ladebildschirme hinzugefügt
- Musik zum Minispiel "Geister Träume" wurde hinzugefügt
- Grafikoptionen hinzugefügt
  - Erfordert einen Neustart des Spiels
- Siegesbildschirm mit Spielzusammenfassung wurde hinzugefügt
- 3 neue Minispieltypen hinzugefügt::
  - Gnu Solo: Einzelspieler-Herausforderungen, die mit einem Gegenstand belohnt werden
  - Nolok Solo: Einzelspieler-Herausforderungen, um zu verhindern, dass ein Kuchen gestohlen wird
  - Nolok Coop: Alle Spieler müssen zusammenarbeiten, um das Minispiel zu gewinnen oder jeder Spieler verliert 10 Kekse
- 3 neue Minispiele hinzugefügt:
  - Waldlauf: Jump and Run platformer (Gnu Solo Minispiel)
  - Verlies Parkour: Jump and run platformer (Nolok Solo Minispiel)
  - Bootsrallye: Steuern Sie ein Boot durch Felsen und vermeiden Sie es, von fallenden Bomben getroffen zu werden (Nolok Coop)
- Credits Bildschirm hinzugefügt

**Verbessert / geändert**

- Reibungslosere Spielerbewegung auf Brettern
- Problem, bei dem der Rollknopf nicht anklickbar war wurde behoben
- Fehlende Übersetzungen behoben
- Verbesserte Konturen bei der Auswahl von Gegenständen / Spielern
- Bessere Erde Textur
- Kuchen erscheinen nicht am selben Ort (wenn möglich)
- Der KI-Schwierigkeitsgrad in Savegames wurde behoben
- Wenn Sie speichern, wenn Sie zur Eingabe eines Minispiels aufgefordert werden, wird das Minispiel beim Laden des Spiels nicht mehr übersprungen
- Absturz beim Laden des Minispiels "Geister Träume" wurde behoben
- Problem, bei dem die Einstellung für Brett Runden ignoriert wurden wurde behoben

