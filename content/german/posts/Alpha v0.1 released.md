---
title: "Alpha v0.1 veröffentlicht"
type: "Post"
date: 2018-09-01T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Die **erste** Alpha-Veröffentlichung von Super Tux Party
mit

- 3 spielbaren Charakteren
- 2 Minispiele
- 2 Belohnungssysteme, der Gewinner nimmt alles und ein lineares
- 1 Brett
- KI-Gegner
- Controller-Neuzuordnung
- Dynamisches Laden von Brettern und Charakteren

