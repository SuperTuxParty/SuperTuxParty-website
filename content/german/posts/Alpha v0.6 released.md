---
title: "Alpha v0.6 veröffentlicht"
type: "Post"
date: 2019-10-29T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Fröhliches Halloween!

Diese neue Version bringt viele neue Funktionen und eine große Überarbeitung bestehender!  
Zunächst wurde das KDEValley-Brett stark überarbeitet! Es entspricht jetzt viel mehr unserem allgemeinen Kunststil.  
Es gibt jetzt drei neue Einstellungen, die du jetzt ändern kannst:

- die Anzahl der Runden, die Sie spielen möchten
- wie viel ein Kuchen kosten sollte
- und die Schwierigkeit der KI-Spieler

Darüber hinaus haben wir ein neues Minispiel im Geiste des bevorstehenden Halloween-Termins veröffentlicht! Es beschäftigt einen völlig neuen Mechaniker. Anstatt gegen die anderen zu spielen, schließen sich die Spieler zusammen, um GNU zu helfen. Diese Minispiele werden durch Landung auf einem GNU-Feld gestarted. Diese Implementierung ist im Moment sehr einfach. Erwarten Sie daher in der nächsten Version eine erheblich verbesserte Version (zusammen mit einem anderen neuen Feld-Typen, aber ich werde sie vorerst geheim halten).

Wie immer ist hier das vollständige Änderungsprotokoll:

**Neue Funktionen**

- New minigame: Haunted dreams
- Barebone Implementierung von Nolok- und GNU-Feldern
- AI-Schwierigkeitsgrade hinzugefügt
- Brett Einstellungen wie Kuchenkosten und Anzahl der Runden können über das Menü überschrieben werden
- Die aktuelle Platzierung der Spieler wird in der Benutzeroberfläche angezeigt
- Kuchenplätze werden verschoben, wenn der Kuchen gesammelt wurde
- Italienische Übersetzung hinzugefügt
- Felder können jetzt als unsichtbar markiert werden, wodurch der Weg beeinflusst werden kann

**Verbessert / geändert**

- Ein 3D-Kuchenmodell wurde hinzugefügt
- Verbessertes Wasser
- Es wurde ein Fehler behoben, der dazu führte, dass der 2v2-Belohnungsbildschirm die falsche Animation abspielte, wenn Team 1 gewann
- Es wurde behoben, dass die Nachricht "Kuchen kaufen" nicht übersetzt wurde
- Das KDEValley-Brett wurde überarbeitet
- Es wurde ein Fehler behoben, der dazu führte, dass das Spiel stecken blieb, wenn ein Brettereignis von einer Grünen Feld nicht behandelt wurde

