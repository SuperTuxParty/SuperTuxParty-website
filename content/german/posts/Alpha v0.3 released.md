---
title: "Alpha v0.3 veröffentlicht"
type: "Post"
date: 2019-02-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Nach langem Warten wird endlich v0.3 für Super Tux Party veröffentlicht, ein freies und Open-Source Partyspiel, das das Gefühl von Spielen wie Mario Party nachbilden soll. Mit über 140 Commits seit dem letzten Update bringt v0.3 viele Änderungen und Verbesserungen, neue Minispiele, ein neues Brett und vieles mehr. Der gesamte Quellcode für diese Version befindet sich im [Hauptzweig auf GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty/tree/master). Genieße es!

## **Alpha-Version - v0.3 - 2019-02-02**

**Neue Funktionen**

- KDEValley, ein neues Brett
- Entkomme der Lava, ein neues Minispiel
- Neuer Hintergrund für das Hauptmenü
- Neue Szenerie für das 'Test' Brett
- Screenshot für das 'Bowling Minispiel' hinzugefügt
- Frame Cap und VSync können jetzt in den Optionen eingestellt werden
- Gegenstände (z. B. Würfel und Fallen)
- Gegenstände können auf Shop Feldern gekauft werden (lila Farbe)

**Verbessert / geändert**

- Jeder Charakter kann jetzt nur einmal ausgewählt werden
- Neues GUI-Thema
- Optionen können jetzt im Spiel geöffnet werden
- Reibungslose Rotationen für Boardbewegungen

Intern wurde das Projekt umstrukturiert und auf den Gitflow Workflow umgestellt.

