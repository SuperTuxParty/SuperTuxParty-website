---
title: "Alpha v0.8 veröffentlicht"
type: "Post"
date: 2020-11-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/d9822d79e61a67392b52c28fb3377ab573b1a8f8f23ba6f16cdf018ae30ae7cb.webp"
---

Hallo allerseits,  
es gibt eine neue SuperTuxParty-Version in der Stadt!  
Wir haben in dieser Version viel am graphischen gearbeitet. Wir haben Cel-Shading hinzugefügt, um den Cartoony-Stil mehr zu erreichen.

**Nicht Cel-shaded**
![Nicht Cel-shaded](/img/58b6c49ded8a709fb7344da0999de8fe7dae2ec37d49dcf65820172d96aa0b0e.webp)

**Cel-shaded**
![Cel-shaded](/img/d9822d79e61a67392b52c28fb3377ab573b1a8f8f23ba6f16cdf018ae30ae7cb.webp)

Wie Sie sehen können, haben wir auch unser Brett Felder in eine sechseckige Form geändert und neue Texturen hinzugefügt, die zu unserem _Cartoon Thema_ passen.

Aber bessere Grafiken sind nicht das einzige, was in dieser Version kommt. Wir haben ein neues Minispiel namens Memory. Dies ist ein 2v2 Minispiel wo Sie und Ihr Partner müssen passende Kartenpaare finden. Das Team, das die meisten von ihnen findet, gewinnt.

![Memory](/img/0abb1b184ae75ee02943e71cb70d9e890b7db48ba4fac0a6834df83663df5b2f.webp)

Wir haben einen Gastgeber für die Boards rekrutiert.

Begrüßen Sie die offizielle Gastgeberin für Super Tux Party, Sara, das [OpenGameArt](https://opengameart.org/)-Maskottchen:

![Sara](/img/6c4d80e321d03931f687072f681955844d8eca7a8b56cd8ef58842a1fb30eed0.webp)

Sie zeigt Ihnen gerne die Bretter und erklärt Ihnen die Regeln, wenn Sie noch nicht mit ihnen vertraut sind.

Zum Schluss möchte ich mich bei unseren freundlichen Übersetzern bedanken. Dank ihrer Bemühungen haben wir Übersetzungen für drei neue Sprachen: Norwegisch Bokmål, Russisch und Türkisch 🎉

Wenn Sie möchten, dass Super Tux Party in Ihre Sprache übersetzt wird, helfen Sie uns bei [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party)

Das war es fürs Erste.
Bis zur nächsten Veröffentlichung!

## **Änderungen v0.8**

**Neue Funktionen**

- Screenshot-Taste (Standard: F2)
- Lizenzen für Shader werden im Credits-Bildschirm angezeigt
- Neues 2v2-Minispiel: Memory
  - Finde passende Kartenpaare!
- Sarah als Host der Bretter hinzugefügt
  - Gibt beim ersten Start ein Tutorial, in dem die grundlegenden Mechanismen erklärt werden
  - Kann übersprungen werden
    - Kündigt an, was auf dem Brett passiert (z. B. wird der Kuchen gekauft und in ein anderes Feld verschoben)
- Übersetzungsfortschritt aus unserem Projekt auf Hosted Weblate
  - Übersetzung für norwegisches Bokmål (98% vollständig) zum Spiel hinzugefügt
  - Übersetzung für Russisch (100% vollständig) zum Spiel hinzugefügt
  - Übersetzung für Türkisch (100% vollständig) zum Spiel hinzugefügt

**Verbessert / geändert**

- Viele visuelle Verbesserungen
  - Neuer Hintergrund beim `Lebensmittel Ernten` Minispiel
  - Added furniture to Haunted Dreams minigame
  - Dem Bowling-Minispiel wurde ein Bowlingbahn-Thema hinzugefügt
    - Boxen fallen vom Himmel,treffen Spieler und blockieren gleichzeitig die Bowlingkugel
    - Einfachere Bewegungssteuerung für den Solo Spieler
  - Bessere und Gamepad-artige agnostische Kontrollsymbole
  - Alles Zellschattiert
  - Sechseckbrettfelder
  - Neue Charakter Splash Kunst
  - Schwebende Felder auf dem KDEValley Brett wurden behoben
  - Bessere Shop-Benutzeroberfläche
- Das Steuerungszuordnungsmenü wurde neu organisiert
  - Erleichtert das Navigieren in den Optionen mit einem Gamepad
- Es wurde ein Fehler behoben, der dazu führte, dass neu hinzugefügte menschliche Spieler nach dem Verlassen und Starten eines neuen Spiels zu KI-Spielern wurden
- Die Randomisierungsalgorithmen vermeiden, dass dasselbe Minispiel mehrmals hintereinander gespielt wird
  - Funktioniert nicht zwischen Sitzungen
- Die Musik des Hauptmenüs wird jetzt abgespielt, nachdem die Audio-Lautstärkeoptionen geladen wurden
