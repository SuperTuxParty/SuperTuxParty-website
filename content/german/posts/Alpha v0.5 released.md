---
title: "Alpha v0.5 veröffentlicht"
type: "Post"
date: 2019-08-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/8f585a88b39b56e16b7217c005403dc7093abecfd92c049da2b917cdda595b7f.webp"
---

Wir sind stolz darauf, unsere fünfte Veröffentlichung nach zwei harten Monaten Arbeit bekannt zu geben.  
Neben vielen kleinen Verbesserungen haben wir volle Unterstützung für Lokalisierung und Hintergrundmusik auf den Brettern und Flucht aus der Lava-Minispiel hinzugefügt.  
Diese Veröffentlichung ist nicht nur eine reguläre Veröffentlichung, da diese Veröffentlichung den ersten Jahrestag des Projekts markiert und ich wage zu sagen, dass wir in diesem einen Jahr große Fortschritte gemacht haben. Schauen Sie sich das Bild unten an und überzeugen Sie sich selbst;)

![One_Year](/img/8f585a88b39b56e16b7217c005403dc7093abecfd92c049da2b917cdda595b7f.webp)

Und das ist nur ein einziges Minispiel! Wir haben auch viele Verbesserungen an den Brettern und Menüs vorgenommen.  
Bevor ich Ihnen das Änderungsprotokoll gebe, möchte ich allen Mitwirkenden für ihre Hilfe bei der Schaffung dieser enormen Leistung danken. Ohne Ihre Unterstützung wäre dieses Projekt nicht dort, wo es heute ist.  
Hier das vollständige Änderungsprotokoll dieser Version:

**Neue Funktionen**

- Unterstützung für die Lokalisierung im Minispiel-Informationsbildschirm
- Unterstützung für die Lokalisierung in Minispielbeschreibungen
- Übersetzte Minispiele
- Die API für Brett-Evente (Grüne Felder) wurde verbessert.
- Musik hinzugefügt zu:
  - Das Test- und KDEValley-Brett
  - Die Flucht aus der Lava-Minispiel

**Verbessert / geändert**

- Fix Französisch Sprache nicht auswählbar
- Die Position des Minispiel-Informationsbildschirms wurde korrigiert, um das gesamte Fenster abzudecken
- Verbesserte Symbolqualität
- Es wurde ein Fehler behoben, durch den das Kuchensymbol auf den Kuchenstellen verschwand
- Ein Absturz wurde behoben, wenn ein Spieler auf einer Falle landete
- Es wurde ein Fehler behoben, der dazu führte, dass der Shop nicht geöffnet wurde, wenn man auf ihn landete
- Die Charaktere im Schneeball stoßen Minispiel sehen jetzt in die Richtung, in die sie gehen
- Das Optionsmenü über das Pausenmenü zugänglich gemacht, sieht wie im Hauptmenü aus
- Die Controller-Navigation in den Optionen wurde verbessert
- Die Charaktere im Erntezeit Minispiel erscheinen nicht mehr in der Luft
- Der schwarze Umriss auf der grünen Tux Textur wurde korrigiert
- Die Beschreibungen in den Zurück-Schaltflächen des Hauptmenüs wurden korrigiert

