---
title: RSS Feed
draft: false
featured_image: "/svg/RSS.svg"
---

Wenn Sie den RSS-Feed für die gesamte Website erhalten möchten **`https://supertux.party/supertuxparty-website/de/index.xml`**

Wenn Sie den RSS-Feed nur für Posts erhalten möchten **`https://supertux.party/supertuxparty-website/de/posts/index.xml`**
