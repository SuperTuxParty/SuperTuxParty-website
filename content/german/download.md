---
title: Download
draft: false
url: /de/download/latest/
featured_image: "/img/6c4d80e321d03931f687072f681955844d8eca7a8b56cd8ef58842a1fb30eed0.webp"
---

## Neueste Version

{{<  download  >}}

[Suchen Sie nach einer anderen Version?](/download/)

## **Änderungen v0.9**

**Verbessert / geändert**

- Der Endbildschirm der Minispiele wurde neu gestaltet
- Neues Kuchendesign
- Hidpi-Unterstützung aktiviert
- Timer für Dungeon Parkour und Forest Run hinzugefügt
- Behoben, dass die KI im Minispiel Escape from Lava stecken blieb
- Minigame-QoL-Änderungen:
  - Die Zeit für die Pflanzenauswahl im Minispiel "Lebensmittel Ernten" wurde von 10 auf 8 Sekunden verkürzt
  - Spawne weniger Bomben im Minispiel "Boat Rally"
  - Vorstart-Countdown in den Minispielen „Escape from Lava“, „Lebensmittel Ernten“ und „Hurdle“ hinzugefügt
  - Minispiel-Screenshots verbessert
  - Bessere Standard-Tastaturbelegungen
- Problem behoben, bei dem das Charaktermodell von Beastie und Godette in den Boden geclippt wurde
- Screenshot-Erstellung unter Windows funktioniert jetzt
- Neues Hauptmenü-Design und Animationen
- Neuer Bildschirm vor den Minispielen
- UI Thema Verbesserungen
- Das Credits-Menü gehorcht jetzt wie vorgesehen dem Thema
- Problem behoben, bei dem das Minispiel „Lebensmittel Ernten“ die Fähigkeit zum Springen auflistete, während das Minispiel dies nicht zulässt
- Zeigen Sie die Steuerelemente, um eine Karte im "Memory" Minispiel umzudrehen
- Das "Kernel-Kompilierungs-Minispiel" wurde überarbeitet
  - Der visuelle Stil entspricht jetzt dem gewünschten Cartoon-Look
  - In der 2v2-Version füllen Teamspieler jetzt denselben Balken statt separater Balken
- Spielersymbole werden jetzt im Minispiel "Memory" korrekt angezeigt
- Fehlerhaftes Verhalten beim Aufdecken einer Karte im Minispiel „Memory“ behoben
- Das Minispiel "Memory" kehrt jetzt nicht sofort zum Endbildschirm des Minispiels zurück, sondern gibt dir etwas Zeit, um dir die Statistiken zu betrachten
- Verwende Liberation Sans anstelle von Noto Sans als Fallback-Schriftart
- Zeige die Gesamtzahl der Runden auf den Brett an
- Die Hintergrundmusik auf dem Testboard wiederholt jetzt
- Das Spiel spielt jetzt einen Ton ab, wenn Sie ein Feld auf dem Brett passieren

## **Änderungen v0.8**

**Neue Funktionen**

- Screenshot-Taste (Standard: F2)
- Lizenzen für Shader werden im Credits-Bildschirm angezeigt
- Neues 2v2-Minispiel: Memory
  - Finde passende Kartenpaare!
- Sarah als Host der Bretter hinzugefügt
  - Gibt beim ersten Start ein Tutorial, in dem die grundlegenden Mechanismen erklärt werden
  - Kann übersprungen werden
    - Kündigt an, was auf dem Brett passiert (z. B. wird der Kuchen gekauft und in ein anderes Feld verschoben)
- Übersetzungsfortschritt aus unserem Projekt auf Hosted Weblate
  - Übersetzung für norwegisches Bokmål (98% vollständig) zum Spiel hinzugefügt
  - Übersetzung für Russisch (100% vollständig) zum Spiel hinzugefügt
  - Übersetzung für Türkisch (100% vollständig) zum Spiel hinzugefügt

**Verbessert / geändert**

- Viele visuelle Verbesserungen
  - Neuer Hintergrund beim `Lebensmittel Ernten` Minispiel
  - Added furniture to Haunted Dreams minigame
  - Dem Bowling-Minispiel wurde ein Bowlingbahn-Thema hinzugefügt
    - Boxen fallen vom Himmel,treffen Spieler und blockieren gleichzeitig die Bowlingkugel
    - Einfachere Bewegungssteuerung für den Solo Spieler
  - Bessere und Gamepad-artige agnostische Kontrollsymbole
  - Alles Zellschattiert
  - Sechseckbrettfelder
  - Neue Charakter Splash Kunst
  - Schwebende Felder auf dem KDEValley Brett wurden behoben
  - Bessere Shop-Benutzeroberfläche
- Das Steuerungszuordnungsmenü wurde neu organisiert
  - Erleichtert das Navigieren in den Optionen mit einem Gamepad
- Es wurde ein Fehler behoben, der dazu führte, dass neu hinzugefügte menschliche Spieler nach dem Verlassen und Starten eines neuen Spiels zu KI-Spielern wurden
- Die Randomisierungsalgorithmen vermeiden, dass dasselbe Minispiel mehrmals hintereinander gespielt wird
  - Funktioniert nicht zwischen Sitzungen
- Die Musik des Hauptmenüs wird jetzt abgespielt, nachdem die Audio-Lautstärkeoptionen geladen wurden

## **Änderungen v0.7**

**Neue Funktionen**

- Ladebildschirme hinzugefügt
- Musik zum Minispiel "Geister Träume" wurde hinzugefügt
- Grafikoptionen hinzugefügt
  - Erfordert einen Neustart des Spiels
- Siegesbildschirm mit Spielzusammenfassung wurde hinzugefügt
- 3 neue Minispieltypen hinzugefügt::
  - Gnu Solo: Einzelspieler-Herausforderungen, die mit einem Gegenstand belohnt werden
  - Nolok Solo: Einzelspieler-Herausforderungen, um zu verhindern, dass ein Kuchen gestohlen wird
  - Nolok Coop: Alle Spieler müssen zusammenarbeiten, um das Minispiel zu gewinnen oder jeder Spieler verliert 10 Kekse
- 3 neue Minispiele hinzugefügt:
  - Waldlauf: Jump and Run platformer (Gnu Solo Minispiel)
  - Verlies Parkour: Jump and run platformer (Nolok Solo Minispiel)
  - Bootsrallye: Steuern Sie ein Boot durch Felsen und vermeiden Sie es, von fallenden Bomben getroffen zu werden (Nolok Coop)
- Credits Bildschirm hinzugefügt

**Verbessert / geändert**

- Reibungslosere Spielerbewegung auf Brettern
- Problem, bei dem der Rollknopf nicht anklickbar war wurde behoben
- Fehlende Übersetzungen behoben
- Verbesserte Konturen bei der Auswahl von Gegenständen / Spielern
- Bessere Erde Textur
- Kuchen erscheinen nicht am selben Ort (wenn möglich)
- Der KI-Schwierigkeitsgrad in Savegames wurde behoben
- Wenn Sie speichern, wenn Sie zur Eingabe eines Minispiels aufgefordert werden, wird das Minispiel beim Laden des Spiels nicht mehr übersprungen
- Absturz beim Laden des Minispiels "Geister Träume" wurde behoben
- Problem, bei dem die Einstellung für Brett Runden ignoriert wurden wurde behoben

## **Änderungen v0.6**

**Neue Funktionen**

- New minigame: Haunted dreams
- Barebone Implementierung von Nolok- und GNU-Feldern
- AI-Schwierigkeitsgrade hinzugefügt
- Brett Einstellungen wie Kuchenkosten und Anzahl der Runden können über das Menü überschrieben werden
- Die aktuelle Platzierung der Spieler wird in der Benutzeroberfläche angezeigt
- Kuchenplätze werden verschoben, wenn der Kuchen gesammelt wurde
- Italienische Übersetzung hinzugefügt
- Felder können jetzt als unsichtbar markiert werden, wodurch der Weg beeinflusst werden kann

**Verbessert / geändert**

- Ein 3D-Kuchenmodell wurde hinzugefügt
- Verbessertes Wasser
- Es wurde ein Fehler behoben, der dazu führte, dass der 2v2-Belohnungsbildschirm die falsche Animation abspielte, wenn Team 1 gewann
- Es wurde behoben, dass die Nachricht "Kuchen kaufen" nicht übersetzt wurde
- Das KDEValley-Brett wurde überarbeitet
- Es wurde ein Fehler behoben, der dazu führte, dass das Spiel stecken blieb, wenn ein Brettereignis von einer Grünen Feld nicht behandelt wurde

## **Änderungen v0.5**

**Neue Funktionen**

- Unterstützung für die Lokalisierung im Minispiel-Informationsbildschirm
- Unterstützung für die Lokalisierung in Minispielbeschreibungen
- Übersetzte Minispiele
- Die API für Brett-Evente (Grüne Felder) wurde verbessert.
- Musik hinzugefügt zu:
  - Das Test- und KDEValley-Brett
  - Die Flucht aus der Lava-Minispiel

**Verbessert / geändert**

- Fix Französisch Sprache nicht auswählbar
- Die Position des Minispiel-Informationsbildschirms wurde korrigiert, um das gesamte Fenster abzudecken
- Verbesserte Symbolqualität
- Es wurde ein Fehler behoben, durch den das Kuchensymbol auf den Kuchenstellen verschwand
- Ein Absturz wurde behoben, wenn ein Spieler auf einer Falle landete
- Es wurde ein Fehler behoben, der dazu führte, dass der Shop nicht geöffnet wurde, wenn man auf ihn landete
- Die Charaktere im Schneeball stoßen Minispiel sehen jetzt in die Richtung, in die sie gehen
- Das Optionsmenü über das Pausenmenü zugänglich gemacht, sieht wie im Hauptmenü aus
- Die Controller-Navigation in den Optionen wurde verbessert
- Die Charaktere im Erntezeit Minispiel erscheinen nicht mehr in der Luft
- Der schwarze Umriss auf der grünen Tux Textur wurde korrigiert
- Die Beschreibungen in den Zurück-Schaltflächen des Hauptmenüs wurden korrigiert

## **Alpha Version - v0.4**

**Neue Funktionen**

- Unterstützung für die Lokalisierung (außer Plugins)
  - Derzeit unterstützte Sprachen:
    - Englisch
    - Brasilianisches Portugiesisch
    - Deutsch
    - Französisch
- Teamanzeige in 2v2-Minispielen

**Verbessert / geändert**

- Das Hauptmenü kann jetzt mit Tastatur / Controller navigiert werden
- Das Brett-Overlay zeigt nun die Gegenstände jedes Spielers
- Computergesteuerte Charaktere kaufen jetzt Artikel im Shop
- Musik im Hauptmenü hinzugefügt
- Behebung eines Fehlers, durch den Elemente in Spielen, die aus Savegames geladen wurden, nicht verwendet werden konnten
- Es wurden Texturen für das Erntezeit Minispiel und die Platzierungsszene hinzugefügt

Intern wurde das Projekt auf die neue Godot-Version 3.1 umgestellt

## **Alpha-Version - v0.3 - 2019-02-02**

**Neue Funktionen**

- KDEValley, ein neues Brett
- Entkomme der Lava, ein neues Minispiel
- Neuer Hintergrund für das Hauptmenü
- Neue Szenerie für das 'Test' Brett
- Screenshot für das 'Bowling Minispiel' hinzugefügt
- Frame Cap und VSync können jetzt in den Optionen eingestellt werden
- Gegenstände (z. B. Würfel und Fallen)
- Gegenstände können auf Shop Feldern gekauft werden (lila Farbe)

**Verbessert / geändert**

- Jeder Charakter kann jetzt nur einmal ausgewählt werden
- Neues GUI-Thema
- Optionen können jetzt im Spiel geöffnet werden
- Reibungslose Rotationen für Boardbewegungen

Intern wurde das Projekt umstrukturiert und auf den Gitflow Workflow umgestellt.

## **Änderungen für v0.2**

**Neue Funktionen**

- Godette, Godots inoffizielles Maskottchen, als neue spielbare Figur **_!53_**
- Animationen für alle Charaktere **_!25_**
- Bowling Minispiel, ein Spieler versucht die 3 anderen Spieler zu treffen **_!63_**
- Kernel Compiling minigame, press the correct buttons quickly **_!52_**
- Erntezeit Minispiel,ein Ratespiel **_!39_**
- Boards can now have multiple paths players can choose which way to go **_!26_**
- Minigame information screens **_!28_**
- New gamemodes added such as, Duel, 1v3 and 2v2 **_!64_**
- Games can now be saved **_!33_**

**Verbesserungen**

- Options are now saved **_!54_**
- Fixed a memory leak **_!29_**
- Improved mesh and texture for ice in Knock Off minigame **_!30_**, **_!34_**
- Hurdle minigame now has powerups and different hurdles **_!38_**

Besonderer Dank geht an RiderExMachina, [Yeldham](https://yeldham.itch.io/) und Dragoncraft89, ohne sie wäre dieses Projekt in so kurzer Zeit nie so weit gekommen!

## Demo Version - v0.1 - 2018-09-01

- 3 spielbare Charaktere
- 2 Minispiele
- 2 Belohnungssysteme, der Gewinner nimmt alles und ein lineares
- 1 Brett
- KI-Gegner
- Controller-Neuzuordnung
- Dynamisches Laden von Brettern und Charakteren

