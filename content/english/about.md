---
title: About
draft: false
featured_image: "/img/6c4d80e321d03931f687072f681955844d8eca7a8b56cd8ef58842a1fb30eed0.webp"
---

## Super Tux Party

The code repository is hosted on [GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty).  
If you have found a bug in the game or have an idea about a feature, please open an issue in the [issue tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/issues).  
See the [README](https://gitlab.com/SuperTuxParty/SuperTuxParty) file for more information.

## Website

This website is under the [GNU GPL v3 license](https://gitlab.com/SuperTuxParty/SuperTuxParty-website/-/blob/master/LICENSE).  
All files for the website are on [GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty-website).

The website is built with [Hugo](https://gohugo.io/) with
the [Ananke theme](https://github.com/theNewDynamic/gohugo-theme-ananke).

The Downloads buttons on the download page are Copyright (c) 2021 by [jesus tapial](https://codepen.io/machuenca/pen/yNLEPL), which are licensed under the [MIT License](/download_button_license/). The source code svg icon on the download page is part of [FontAwesome](https://fontawesome.com/) and licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).

The Feed icon on the RSS page is from unnamed (Mozilla Foundation) - feedicons.com,GPL 2.0 or any later version, https://commons.wikimedia.org/w/index.php?curid=654420
and is downloaded from [Wikipedia Commons](https://commons.wikimedia.org/wiki/File:Feed-icon.svg)

The GitLab icon is licensed under the [CC BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/) by GitLab.

---
## Responsible for content on this site

**Name**: Florian Kothmeier

**Address**: Fahrstraße 6,  
91054 Erlangen, Germany

**E-mail**: [info@supertux.party](mailto:info@supertux.party)
