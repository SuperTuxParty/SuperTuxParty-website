---
description: "A free/libre and open-source party game."
---

#### **NOTE: This game is currently in the ALPHA phase**

A [free/libre](https://www.gnu.org/philosophy/free-sw.html) and
[open-source](https://opensource.org/docs/osd/) party game that is meant to
replicate the feel of games such as Mario Party.

The code repository for [the game](https://gitlab.com/SuperTuxParty/SuperTuxParty) and for [the site](https://gitlab.com/SuperTuxParty/SuperTuxParty-website) are on GitLab
