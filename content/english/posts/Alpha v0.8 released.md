---
title: "Alpha v0.8 released"
type: "post"
date: 2020-11-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/d9822d79e61a67392b52c28fb3377ab573b1a8f8f23ba6f16cdf018ae30ae7cb.webp"
---

Hello everyone,  
there's a new Super Tux Party release in town!
We've been working a lot on graphics in this release. We've added cel-shading, to get a more cartoony style.

**Not cel shaded**
![Not cel shaded](/img/58b6c49ded8a709fb7344da0999de8fe7dae2ec37d49dcf65820172d96aa0b0e.webp)

**Cel shaded**
![Cel shaded](/img/d9822d79e61a67392b52c28fb3377ab573b1a8f8f23ba6f16cdf018ae30ae7cb.webp)

As you can see, we've also changed our board fields to a hexagonal shape and added new textures that fit our _cartoony theme_.

But better graphics are not the only thing that's coming in this release. We've got a new minigame called Memory. This is a 2v2 minigame where you and your partner need to find matching card pairs. The team that finds the most of them wins.

![Memory](/img/0abb1b184ae75ee02943e71cb70d9e890b7db48ba4fac0a6834df83663df5b2f.webp)

We've recruited a host for the boards.

Welcome the official hostess for Super Tux Party, Sara, the [OpenGameArt](https://opengameart.org/) mascot:

![Sara](/img/6c4d80e321d03931f687072f681955844d8eca7a8b56cd8ef58842a1fb30eed0.webp)

She will be happy to show you around the board and explain the rules if you're not already familiar with them.

At last, I want to thank our kind translators. Thanks to their effort, we've got translations for three new languages: Norwegian Bokmål, Russian and Turkish 🎉

If you want to see Super Tux Party translated to your language, then lend us a hand on [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party)

That's it for now.
See you next release!

## **Changelog v0.8**

**New features**

- Screenshot key (Default: F2)
- Show licenses for shaders in the credits screen
- New 2v2 minigame: Memory
  - Find matching card pairs!
- Added Sarah as the boards' host
  - Gives tutorial on the first start, explaining the basic mechanics
    - Can be skipped
  - Announces what happens on the board (e.g. the cake is bought and moves to another space)
- Translation progress from our project on Hosted Weblate
  - Added Translation for Norwegian Bokmål (98% complete) to the game
  - Added Translation for Russian (100% complete) to the game
  - Added Translation for Turkish (100% complete) to the game

**Improved / Changed**

- Lots of visual improvements
  - New Harvest Food minigame background
  - Added furniture to Haunted Dreams minigame
  - Added a bowling alley theme to the Bowling minigame
    - Boxes fall from the sky, stunning players hit, while also blocking the bowling ball
    - Easier movement controls for the solo player
  - Better and gamepad type agnostic control icons
  - Cell shaded everything
  - Hexagon board fields
  - New character splash art
  - Floating fields on the KDEValley board have been fixed
  - Better Shop UI
- Reorganized the control mapping menu
  - Makes it easier to navigate the options with a gamepad
- Fixed a bug that caused newly added human players to become AI players after leaving and starting a new game
- The randomization algorithms avoids playing the same minigame multiple times in a row
  - Doesn't work between sessions
- The main menu music now starts to play after the audio volume options are loaded
