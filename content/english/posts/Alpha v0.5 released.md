---
title: "Alpha v0.5 released"
type: "post"
date: 2019-08-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/8f585a88b39b56e16b7217c005403dc7093abecfd92c049da2b917cdda595b7f.webp"
---

We're proud to announce our fifth release after two hard months of work.  
Besides many minor improvements, we have full support for localization and background music on the boards and the escape from lava minigame.  
This release is not just a regular release, because this release marks the first anniversary of the project and I dare to say that we have made a lot of progress in this one year. Take a look at the picture below and see for yourself ;)

![One_Year](/img/8f585a88b39b56e16b7217c005403dc7093abecfd92c049da2b917cdda595b7f.webp)

And this is only a single minigame! We've had many improvements to the boards and the menus as well.  
Before I'll give you the changelog, I want to thank all contributors for their help to create this enormous achievement, this project wouldn't be where it is today without your support.  
At last, here is the full changelog of this release:

**New features**

- Support for localization in the minigame information screen
- Support for localization in minigame descriptions
- Translated minigames
- Improved the API for board events (green spaces)
- Add music to:
  - The test and KDEValley boards
  - The Escape from lava minigame

**Improved / Changed**

- Fix the French language not selectable
- Fixed the position of the minigame information screen to cover the full window
- Improved icon quality
- Fixed a bug that caused the cake icon on cake spots to disappear
- Fixed a crash when the player landed on a trap
- Fixed a bug that caused the shop not to open when landing directly on it
- The characters in the knock off minigame now face the direction they are walking
- Made the options menu accessible from the pause menu look like in the main menu
- Improved the controller navigation in the options
- The characters in the harvest food minigame no longer spawn in the air
- Fixed the black outline on the green tux texture
- Fixed the descriptions in the main menu back buttons
