---
title: "Alpha v0.6 released"
type: "post"
date: 2019-10-29T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

Happy Halloween!

This new release brings a lot of new features and a big rework of existing ones!  
First of all, the KDEValley board was heavily revised! It's much more in line with our general art style now.  
These are three new settings. You can change:

- the number of turns you want to play
- how much a cake should cost
- and the difficulty of AI players

On top of this, we released a new minigame in the spirit of the upcoming Halloween date! It employs an entirely new mechanic. Instead of playing versus the others, the players join forces to help GNU. These minigames are initiated by landing on a GNU space. This implementation is very basic at the moment, so expect to see a vastly improved version in the next release (Along with another new space type, but I'll keep it a secret for now).

As always, here is the full changelog:

**New features**

- New minigame: Haunted dreams
- Barebone implementation of Nolok and GNU spaces
- Added AI difficulty levels
- Board settings, such as Cake cost and number of turns can be overridden via the menu
- The current placement of players is shown in the UI
- Cake spaces get relocated, when the cake is collected
- Add Italian translation
- Spaces can now be marked as invisible, which can be used to influence the walking path

**Improved / Changed**

- Added a 3D cake model
- Improved water
- Fixed a bug that caused the 2v2 reward screen to play the wrong animation if Team1 wins
- Fixed the buy cake message not being translated
- Reworked the KDEValley board
- Fixed a bug that caused the game to get stuck, when a board event from a green space was not handled

