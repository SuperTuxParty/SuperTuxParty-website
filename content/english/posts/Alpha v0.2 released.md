---
title: "Alpha v0.2 released"
type: "post"
date: 2018-10-31T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

After two months of work, our new update is finally here! It includes a lot of new features and changes including a new playable character, Godette. Three new minigames and new game modes have also been added which is exciting so please test it out! Currently, you will notice that we are missing quite a few assets and we are in search of an artist, if you want to help out or know a person that could help us out, please reach out to us on our [GitLab](https://gitlab.com/supertuxparty/supertuxparty) or our [Matrix channel](https://matrix.to/#/!hDKeDHVhkMRnxkNyga:matrix.org).

If you have any issues, please report them on our [issue tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/issues)!

## **Changelog for v0.2**

**New features**

- Godette, Godot's unofficial mascot, as a new playable character **_!53_**
- Animations for all characters **_!25_**
- Bowling minigame, one player tries to hit the other 3 players **_!63_**
- Kernel Compiling minigame, press the correct buttons quickly **_!52_**
- Harvest Food minigame, guessing game **_!39_**
- Boards can now have multiple paths, players can choose which way to go **_!26_**
- Minigame information screens **_!28_**
- New game modes added such as Duel, 1v3, and 2v2 **_!64_**
- Games can now be saved **_!33_**
  **Improved**
- Options are now saved **_!54_**
- Fixed a memory leak **_!29_**
- Improved mesh and texture for ice in Knock Off minigame **_!30_**, **_!34_**
- Hurdle minigame now has powerups and different hurdles **_!38_**

Special thanks to RiderExMachina, [Yeldham](https://yeldham.itch.io/) and Dragoncraft89, without them this project would never have reached this far in such a short amount of time!

