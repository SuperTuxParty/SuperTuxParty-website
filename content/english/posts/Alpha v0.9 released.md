---
title: "Alpha v0.9 released"
type: "post"
date: 2021-04-19T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/48b9e74e8df51e6670f3c8d351d28e2b0005e60e590457a8649aa7a028a9ccb0.webp"
---

We're back with a new Super Tux Party release.

This release focuses on polishing the game instead of bringing a ton of new features.

The reason for this is that we’re going to start working on online multiplayer, as this has been one of our very first feature requests and we are at a point by now, where it makes sense to implement it. Because this is a big feature, we want to have the game in a good state for this.

Now let’s take a look at the big changes in this release.

We’ve revamped the pre-minigame screen with a much improved design.

![New pre-minigame screen](/img/7f23c35e32d03a9b1488be622910b7bcf7093e7edd0ddd4075364fdd43ae115e.webp)

Besides a much improved layout, it also shows you what minigame type you’re about to play, the team colors and only displays relevant controls for 1v3 minigames.

We also have redesigned the minigame end screen you see, after you’ve played a minigame.

![Redesigned minigame end-screen](/img/eb1e5d76c21723b5bdcc23a88048290cc7742400e7fc92893448560b8c29ed3a.webp)

It's much more visually pleasing and you can see the rewards each player got for their placement. Also the screen just moves on, when all players are ready. So you have as much time as you need to look at the stats.

We also have a new main menu, to replace the old one, to give the game more character.

![New main menu](/img/48b9e74e8df51e6670f3c8d351d28e2b0005e60e590457a8649aa7a028a9ccb0.webp)

And a complete new look for the "Kernel compiling" minigame as well.

![New look](/img/8aa7a7d83687fe6fcb479d5ade116a7bbbbdb511669fe5296cf94066e4c61bbc.webp)

We also changed the way the 2v2 variant works here. Instead of each player filling their own bar, each team has only one bar they must fill. To transform this variant into a real team game.

Also a big shout-out to franzopow, for doing the music in the “Kernel compiling” minigame as well as in the post-minigame screen. If you like his work, consider supporting him at [paypal](https://www.paypal.com/donate?hosted_button_id=SZUUK34MBU944).

As always here’s the full changelog:

## **Changelog v0.9**

**Improved / Changed**

- Redesigned the minigame end screen
- New cake design
- Enabled hidpi support
- Add a timer for Dungeon Parkour and Forest Run
- Fix AI getting stuck in Escape from Lava minigame
- Minigame QoL changes:
  - Decreased the time for plant selection in "Harvest Food" minigame from 10 seconds to 8 seconds
  - Spawn less Bombs in "Boat Rally" minigame
  - Added pre-start countdown in "Escape from Lava", "Harvest Food" and "Hurdle" minigames
  - Updated minigame screenshots
  - Provide better default keybindings
- Fixed Beastie's and Godette's character model clipping into the ground
- Screenshot creation on Windows works now
- New Main Menu design and animations
- New Pre-minigame screen
- UI theme improvements
- Credits menu now properly obeys the theme
- Fixed "Harvest Food" minigame listing the ability to jump, while the minigame doesn't allow it
- Show the controls to flip a card in the "Memory" minigame
- Reworked the "Kernel compiling minigame"
  - The visual style now matches the desired cartoony look
  - In the 2v2 version, team players now fill the same bar instead of separate bars
- Player icons now show up correctly in the "Memory" minigame
- Fixed some buggy behavior when flipping a card faceup in the "Memory" minigame
- The "Memory" minigame now does not return instantly to the minigame end screen, but gives you a bit of time to look at the stats
- Use Liberation Sans instead of Noto Sans as fallback font
- Show the total number of turns on the board
- The Background music on the test board loops now
- The game now plays a sound when you pass a space on the board
