---
title: "Alpha v0.4 released"
type: "post"
date: 2019-06-08T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

This update polishes several aspects of Super Tux Party. We've been concentrating on adding missing assets, such as backgrounds to the game.  
Other than that, we've got translations, music in the main menu, and some bug fixes.  
If you've got ideas for improvement, want to create some art for the game, or contribute otherwise, head over to our GitLab.

## **Alpha Version - v0.4**

**New features**

- Support for localization (except plugins)
  - Currently supported languages:
    - English
    - Brazilian Portuguese
    - German
    - French
- Team indicator in 2v2 minigames

**Improved / Changed**

- The main menu can now be navigated with a keyboard/controller
- The board overlay now shows the items of each player
- Computer-controlled characters now buy items in the shop
- Added Music in the main menu
- Fix a bug that made items not usable in games loaded from savegames
- Added textures for Harvest Food minigame and placement scene

Internally the project has switched to the new Godot version 3.1
