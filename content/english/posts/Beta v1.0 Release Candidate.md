---
title: "Beta v1.0 Release Candidate"
date: 2022-06-26T12:00:00+02:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/cf54ee509ea4aa91cf60afbb32aa3251a6761af47c2e2559a075de3a3d877173.webp"
---

After a long delay, we can finally present our progress. Alas it's not a complete release yet, but we feel confident enough to present our progress and get feedback on the implementation.

So now what's the big deal that has taken so long to? The answer is: Online Multiplayer. So now you can finally play with your friends even from the other side of the world!

Also, we improved our game UI to support online multiplayer:

![multiplayer lobby](/img/cf54ee509ea4aa91cf60afbb32aa3251a6761af47c2e2559a075de3a3d877173.webp)

If you want to check it out, head over to our [downloads page](/download/latest) and download the new version. We have a server running on `supertux.party` you can use.

If you want to spin up a SuperTuxParty server yourself, we have a new download option for "server", which you can use to do so.

Furthermore, if you encounter any bugs, be sure to tell us about them, so we can fix them! You can report bugs via our [gitlab issue tracker](https://gitlab.com/SuperTuxParty/SuperTuxParty/-/issues)

