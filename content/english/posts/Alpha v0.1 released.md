---
title: "Alpha v0.1 released"
type: "post"
date: 2018-09-01T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

The **first** Alpha release of Super Tux Party  
with

- 3 playable characters
- 2 minigames
- 2 reward systems, winner takes all and a linear one
- 1 board
- AI opponents
- Controller remapping
- Dynamic loading of boards & characters

