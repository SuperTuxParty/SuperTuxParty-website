---
title: "Alpha v0.3 released"
type: "post"
date: 2019-02-02T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
---

After a long wait, v0.3 is finally released for Super Tux Party, a free/libre and open-source party game that is meant to replicate the feel of games such as Mario Party. With over 140 commits since the last update, v0.3 brings a lot of changes and improvements, new minigames, a new board and much more. All source code for this release can be found on the [master branch on GitLab](https://gitlab.com/SuperTuxParty/SuperTuxParty/tree/master). Enjoy!

## **Alpha-Version - v0.3 - 2019-02-02**

**New features**

- KDEValley, a new board
- Escape from lava, a new minigame
- New background for the main menu
- New scenery for 'test' board
- Added screenshot for 'Bowling minigame'
- Frame cap and VSync can now be set in options
- Items (e.g. Dice and traps)
- Items can be bought in Shop Spaces (purple color)

**Improved / Changed**

- Each character can only be chosen once now
- New GUI theme
- Options can now be opened in-game
- Smooth rotations for board movement

Internally the project has been restructured and switched to the Gitflow Workflow.

