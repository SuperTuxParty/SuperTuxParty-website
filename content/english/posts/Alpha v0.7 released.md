---
title: "Alpha v0.7 released"
type: "post"
date: 2020-04-01T00:00:00+01:00
draft: false
show_reading_time: true
toc: true
featured_image: "/img/87c6c8c5d5cc42506d2052355c81f3c29d0a3378a540adb35bb1de90a7fc67a6.webp"
---

Today may be the first of April, but this is not a joke:  
**There is a new Super Tux Party release!**

As announced in the previous version, this version contains a vastly improved version of GNU fields and a new field: Nolok fields.

## **How does it work?**

### **GNU fields**

![GNU fields](/img/87c6c8c5d5cc42506d2052355c81f3c29d0a3378a540adb35bb1de90a7fc67a6.webp)

When landing on a GNU field, a minigame will be played.

There a two types of minigames:

- Solo minigames: A single-player challenge. If the player succeeds, the player will be given an item as a reward.
- Coop minigames: Here the players have to join forces. If they succeed they will each get 10 cookies.

So GNU fields help you and all other players.

### **Nolok fields**

![Nolok fields](/img/87c6c8c5d5cc42506d2052355c81f3c29d0a3378a540adb35bb1de90a7fc67a6.webp)

Nolok fields are the opposite of GNU fields. As GNU fields reward the players, Nolok fields punish them instead.

Nolok fields invoke different effects when landed on, too:

- Solo minigames: Very similar to the GNU variant, just instead of rewarding the player with an item. They will lose a cake if they fail the minigame.
- Coop minigames: Also similar to the GNU variant, but the players are not given cookies if they succeed, they lose cookies if they fail.
- Board effect: Contrary to the GNU fields, Nolok fields can give the player a punishment without playing a minigame. Currently, there are two types of punishments:
  - Stealing coins: Nolok will steal the player's coins depending on their placement
  - Bad rolls: Nolok can also worsen the dice rolls of players. If this happens, your dice rolls will be 2 less every roll for the next 5 turns.

So Nolok fields give quite bad effects when you land on them. Better stay away from them!

## **New minigames**

As we've introduced new minigame types, we need of course minigames to implement these new types! The previous version of gnu minigames are now called GNU Coop minigames, so there are 3 new minigames we need to discuss.

### **Forest Run (GNU Solo):**

![Forest Run](/img/837496cd40cdf6b1caa0b6007b4a5bd3d93f3aad183f60664285ca96ecde1f7f.webp)

This is a Jump and Run style minigame, where you have to dodge rocks thrown by catapults and jump between platforms to reach the finish line.

### **Dungeon Parcour (Nolok Solo):**

![Dungeon Parcour](/img/0c91298184bca27770e6fc0d73b93c3db4a53a735d2442cbeeb5e51deeafd2d9.webp)

Similar to the forest run minigame, this minigame is also a Jump and Run style minigame. You need to dodge fireballs instead of thrown rocks.

### **Boat Rally (Nolok Coop):**

![Boat Rally](/img/5c221ccd2c0f6710958dc9d986ed5dea8bf1d61be82d272a609920fe6e67fbf6.webp)

In this minigame, you need to reach the finish line with your boat. The catch is that each player can only paddle at his position.
Therefore teamwork is important here.

## **Translations**

We've been kindly given a free hosting solution for translation on [Hosted Weblate](https://hosted.weblate.org/projects/super-tux-party/). Translations are open and welcome, so please help out if you can!

## **Changelog v0.7**

**New features**

- Added loading screens
- Added music to "Haunted Dreams" minigame
- Added Graphic options
  - Requires game restart
- Added victory screen with a game summary
- Added 3 new minigame types:
  - Gnu Solo: Single-player challenges that are rewarded with an item
  - Nolok Solo: Single-player challenges to prevent a cake from being stolen
  - Nolok Coop: All players must work together to win the minigame or each player will lose 10 cookies
- Added 3 new minigames:
  - Forest Run: Jump and run platformer (Gnu Solo minigame)
  - Dungeon Parcour: Jump and run platformer (Nolok Solo minigame)
  - Boat Rally: Steer a boat through rocks and avoid being hit by falling bombs (Nolok Coop)
- Added Credits screen

**Improved / Changed**

- Smoother player movement on boards
- Fixed roll button not clickable
- Fixed missing translations
- Improved outlines when selecting items/players
- Better dirt texture
- Cakes don't respawn at the same location (if possible)
- The AI difficulty level in savegames has been fixed
- If you save when prompted with a mini-game, the mini-game will no longer be skipped when the game is loaded
- Fixed crash when loading the "Haunted Dreams" minigame
- Fixed the board turns setting being ignored

