---
title: RSS Feed
draft: false
featured_image: "/svg/RSS.svg"
---

If you want to receive the RSS Feed for the entire website **`https://supertux.party/supertuxparty-website/index.xml`**

If you want to receive the RSS Feed only for Posts **`https://supertux.party/supertuxparty-website/posts/index.xml`**
