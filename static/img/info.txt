The names of the files are the sha256 hashes generated with the following command

find . -maxdepth 1 -name '*.webp' -print0 | while read -d $'\0' i ;do     mv "$i" "`sha256sum "$i" | cut -d" " -f1`.webp"; done